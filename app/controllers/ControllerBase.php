<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {
        // Seta o  layout do sistema
        $this->view->setMainView('/mainView/index');
    }
}
