<?php

class ProdutoController extends ControllerBase
{
    public function indexAction()
    {
        return $this->response->redirect('produto/listar');
    }
    
    public function cadastroAction()
    {
        $this->tag->setDefault('tipo', \Library\Db\Produto::TIPO_CADASTRO);
        $this->view->setVar('img', null);
        $this->view->setVar('categorias', \Model\ProdutoCategoria::find(["order" => "nome"]));
        $this->view->pick("produto/form");
    }
    
    public function listarAction()
    {
        $params = [];
        
        $request = $this->request;
        if ($request->isPost())
            $params = $request->getPost();
        
        $produtos = \Library\Db\Produto::listar($params);
        $this->view->setVar('produtos', $produtos);
    }
    
    public function editarAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            $produtoId = $request->getPost('produto_id');
            $dbProduto = \Model\Produto::findFirst($produtoId);
            if (!$dbProduto) {
                $this->flash->error("Produto não encontrado");
                return $this->response->redirect('produto/listar');
            }
            
            $this->tag->setDefaults([
                'tipo' => \Library\Db\Produto::TIPO_UPDATE,
                'categoria_id' => $dbProduto->getCategoriaId(),
                'descricao' => $dbProduto->getDescricao(),
                'caracteristicas' => $dbProduto->getCaracteristicas(),
                'valor' => $dbProduto->getValor(),
                'produto_id' => $produtoId,
            ]);
            
            $this->view->setVar('img', $dbProduto->getImg());
            $this->view->setVar('categorias', \Model\ProdutoCategoria::find(["order" => "nome"]));
            $this->view->pick("produto/form");
        }
    }
    
    public function excluirAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            $produtoId = $request->getPost('produto_id');
            $dbProduto = \Model\Produto::findFirst($produtoId);
            
            if (!$dbProduto) {
                $this->flash->error("Produto não encontrado");
                return $this->response->redirect('produto/listar');
            }
            
            if($dbProduto->getImg() != null && file_exists('files/produto/' . $dbProduto->getImg()))
                unlink('files/produto/' . $dbProduto->getImg());
            
            unlink('files/produto/' . $dbProduto->getImg());
            $dbProduto->delete();
            $this->flash->success("Produto excluído com sucesso!");
            return $this->response->redirect('produto/listar');
        }
    }
    
    public function salvarAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            
            $data = $request->getPost();
            $data['valor'] = \Library\Db\Produto::replaceValor($data['valor']);
            $dbProduto = \Library\Db\Produto::salvar($data, $data['tipo'], $this->request->getUploadedFiles());
            
            if($dbProduto['save']) {
                $mensagem = 'Produto %TIPO com sucesso!';
                $mensagem = str_replace('%TIPO', $data['tipo'] == 1 ? 'cadastrado' : 'atualizado', $mensagem);
                $this->flash->success($mensagem);
                return $this->response->redirect('produto/listar');
            } else {
                $mensagem = \Model\Produto::mensagensValidacao($dbProduto['messages']->getMessages());
                foreach ($mensagem as $mensage)
                    $this->flash->error($mensage);
                
                return $this->response->redirect('produto/cadastro');
            }
        }
    }
    
    public function visualizarAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            $produtoId = $request->getPost('produto_id');
            $dbProduto = \Model\Produto::findFirst($produtoId);
            if (!$dbProduto) {
                $this->flash->error("Produto não encontrado");
                return $this->response->redirect('index');
            }
            
            $this->view->setVar('produto', $dbProduto);
        }
    }
}