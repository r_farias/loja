<?php

class ErrorController extends ControllerBase
{
    public function notFoundAction()
    {
        
    }
    
    public function uncaughtExceptionAction()
    {
        $this->view->setVar('e', $this->dispatcher->getEventsManager());
    }
}
