<?php

class IndexController extends ControllerBase
{
    public function indexAction()
    {
        $params = [];
        
        $request = $this->request;
        
        if ($request->isPost())
            $params = $request->getPost();
        
        $produtos = \Library\Db\Produto::listar($params);
        $this->view->setVar('produtos', $produtos);
    }
}

