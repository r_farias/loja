<?php

class ProdutoCategoriaController extends ControllerBase
{
    public function indexAction()
    {
        return $this->response->redirect('produto-categoria/listar');
    }
    
    public function cadastroAction()
    {
        $request = $this->request;
        $this->tag->setDefault('tipo', \Library\Db\Produto::TIPO_CADASTRO);
        $this->view->pick("produto-categoria/form");
    }
    
    public function listarAction()
    {
        $params = [];
        
        $request = $this->request;
        if ($request->isPost())
            $params = $request->getPost();
        
        $categorias = \Library\Db\ProdutoCategoria::listar($params);
        $this->view->setVar('categorias', $categorias);
    }
    
    public function editarAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            $categoriaId = $request->getPost('categoria_id');
            $dbProdutoCategoria = \Model\ProdutoCategoria::findFirst($categoriaId);
            if (!$dbProdutoCategoria) {
                $this->flash->error("Categoria não encontrada");
                return $this->response->redirect('produto-categoria/listar');
            }
            
            $this->tag->setDefaults([
                'tipo' => \Library\Db\Produto::TIPO_UPDATE,
                'nome' => $dbProdutoCategoria->getNome(),
                'categoria_id' => $dbProdutoCategoria->getCategoriaId(),
            ]);
            
            $this->view->pick("produto-categoria/form");
        }
    }
    
    public function salvarAction()
    {
        $request = $this->request;
        if ($request->isPost()) {
            
            $data = $request->getPost();
            $dbProdutoCategoria = \Library\Db\ProdutoCategoria::salvar($data, $data['tipo']);
            
            if($dbProdutoCategoria['save']) {
                $mensagem = 'Categoria %TIPO com sucesso!';
                $mensagem = str_replace('%TIPO', $data['tipo'] == 1 ? 'cadastrado' : 'atualizado', $mensagem);
                $this->flash->success($mensagem);
                return $this->response->redirect('produto-categoria/listar');
            } else {
                $mensagem = \Model\ProdutoCategoria::mensagensValidacao($dbProdutoCategoria['messages']->getMessages());
                foreach ($mensagem as $mensage)
                    $this->flash->error($mensage);
                
                return $this->response->redirect('produto-categoria/cadastro');
            }
        }
    }
}