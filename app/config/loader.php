<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir
    )
);
        
//Register some namespaces
$loader->registerNamespaces(
    array(
       "Model" => $config->application->modelsDir,
       "Library" => $config->application->libraryDir,
    )
);

$loader->register();
