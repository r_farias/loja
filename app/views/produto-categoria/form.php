<div class="row">
    <fieldset>
        <legend>Cadastro de Categoria</legend>
        <form id="form-categoria" action="<?=$this->url->get('produto-categoria/salvar')?>" method="POST">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label"  for="nome">Nome</label>
                        <?=$this->tag->textField(["nome", "class" => 'form-control', "placeholder" => "Nome da categoria"])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="<?=$this->url->get('produto-categoria/listar')?>" class="btn btn-default">Voltar</a>
                    <button class="btn btn-success" type="submit">Salvar</button>
                </div>
            </div>
            <?=$this->tag->hiddenField("tipo")?>
            <?=$this->tag->hiddenField("categoria_id")?>
        </form>
    </fieldset>
</div>
<script>
    $(function() {
        var formCategoria = {
            nome: {
                required: true
            }
        }
        
         $('#form-categoria').validate({
            rules: formCategoria,
            messages: {
                nome: {
                    required: 'Digite o nome da categoria',
                },
            },
            highlight: function(element) {
                $(element).closest('div.form-group').addClass("has-error");
                $(element).closest('div.form-group').find('label:first').addClass("control-label");
            },
            unhighlight: function(element) {
                $(element).closest('div.form-group').removeClass("has-error");
            }
        });
    })
</script>