<div class="row margin-div">
    <div class="col-md-12">
        <a href="<?=$this->url->get('produto-categoria/cadastro')?>" class="btn btn-default">Nova Categoria</a>
    </div>
</div>
<fieldset>
    <legend>Lista de Categorias Cadastradas</legend>
    <form method="POST" action="">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>Nome da Categoria</th> 
                    <th style="width: 20%">Ações</th> 
                </tr> 
            </thead> 
            <tbody>
                <tr>
                    <td>
                        <?=$this->tag->textField(["nome", "class" => 'form-control'])?>
                    </td>
                    <td>
                        <button class="btn btn-default btn-sm" type="submit">
                            Pesquisar
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-default btn-sm" type="reset" value="Reset">
                            Limpar
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
                <?php if(count($categorias) > 0) { ?>
                    <?php foreach ($categorias as $item) { ?>
                    <tr> 
                        <td><?=$item->getNome()?></td> 
                        <td>
                            <button type="button" class="btn btn-sm btn-primary" name="btn-acao" value="<?=$item->getCategoriaId()?>">
                                Editar
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                        </td> 
                    </tr> 
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="5">Nenhum registro encontrado</td></tr> 
                <?php } ?>
            </tbody> 
        </table>
    </form>
</fieldset>
<div class="modal fade" tabindex="-1" id="modal-confirmacao" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content text-center">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button> 
                <h4 class="modal-title" id="mySmallModalLabel">Confirmação</h4> 
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja excluir este produto?</p>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default alg-left-btn" data-dismiss="modal">Cancelar</button> 
                <button type="button" name="btn-excluir" class="btn btn-danger">Excluir</button> 
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('button[name="btn-acao"]').click(function() {
            
            var params = {
                action: '<?=$this->url->get('produto-categoria/editar')?>',
                method: 'POST',
                inputs: {
                    categoria_id: $(this).val()
                }
            }
            
            App.createFormSubmit(params);
        });
    });
</script>
    