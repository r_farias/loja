<div class="jumbotron">
    <h1>Bem-vindo a Loja RF</h1>
    <p>Veja abaixo nossos produtos</p>
</div>
<div class="row">
    <?php if (count($produtos) > 0) { ?>
        <?php foreach($produtos as $produto) { ?>
            <div class="col-md-4 painel-produto">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-5">
                            <?php $img = ($produto->produto->getImg() != null) ? 'files/produto/' . $produto->produto->getImg() : 'img/produto-default.png'?>
                            <img src="<?=$this->url->getBaseUri() . $img?>" class="img-produto">
                        </div>
                        <div class="col-md-7">
                            <h4><?=$produto->produto->getDescricao()?></h4>
                            <p>Por <?= $produto->produto->getValor()?></p>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button name="btn-visualizar" value="<?=$produto->produto->getProdutoId()?>" class="btn btn-default btn-block">Ver detalhes</button>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<script>
    $(function() {
        $('button[name="btn-visualizar"]').click(function() {
            var params = {
                action: '<?=$this->url->get('produto/visualizar')?>',
                method: 'POST',
                inputs: {
                    produto_id: $(this).val()
                }
            }
            
            App.createFormSubmit(params);
        });
    });
</script>