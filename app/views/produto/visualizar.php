<div class="row">
    <div class="col-md-4">
        <?php $img = ($produto->getImg() != null) ? 'files/produto/' . $produto->getImg() : 'img/produto-default.png'?>
        <img src="<?=$this->url->getBaseUri() . $img?>" class="img-responsive">
    </div>
    <div class="col-md-8">
        <h3><?=$produto->getDescricao()?></h3>
        <p>
            <b>Caracteristicas do produto</b><br/>
            <?=$produto->getCaracteristicas()?>
        </p>
        <h3><?=$produto->getValor()?></h3>
    </div>
</div>