<div class="row margin-div">
    <div class="col-md-12">
        <a href="<?=$this->url->get('produto/cadastro')?>" class="btn btn-default">Novo Produto</a>
    </div>
</div>
<fieldset>
    <legend>Lista de Produtos Cadastrados</legend>
    <form method="POST" action="">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>Descrição</th> 
                    <th>Caracteristicas</th> 
                    <th>Valor</th> 
                    <th>Categoria</th> 
                    <th style="width: 20%">Ações</th> 
                </tr> 
            </thead> 
            <tbody>
                <tr>
                    <td>
                        <?=$this->tag->textField(["descricao", "class" => 'form-control'])?>
                    </td>
                    <td>
                        <?=$this->tag->textField(["caracteristicas", "class" => 'form-control'])?>
                    </td>
                    <td>
                        <?=$this->tag->textField(["valor", "class" => 'form-control'])?>
                    </td>
                    <td>
                        <?php
                            echo $this->tag->select([
                                'categoria_id',
                                \Model\ProdutoCategoria::find(["order" => "nome"]),
                                "using" => array("categoria_id", "nome"),
                                "class" => 'form-control',
                                'useEmpty' => true,
                                'emptyText' => 'Todos',
                                'emptyValue' => 'todos',
                            ]);
                        ?>
                    </td>
                    <td>
                        <button class="btn btn-default btn-sm" type="submit">
                            Pesquisar
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </button>
                        <button class="btn btn-default btn-sm" type="reset" value="Reset">
                            Limpar
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                        </button>
                    </td>
                </tr>
                <?php if(count($produtos) > 0) { ?>
                    <?php foreach ($produtos as $item) { ?>
                    <tr> 
                        <td><?=$item->produto->descricao?></td> 
                        <td><?=$item->produto->caracteristicas?></td> 
                        <td><?=$item->produto->valor?></td> 
                        <td><?=$item->nome?></td> 
                        <td>
                            <button type="button" class="btn btn-sm btn-primary" data-action="1" name="btn-acao" value="<?=$item->produto->produto_id?>">
                                Editar
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            </button>
                            <button type="button" class="btn btn-sm btn-danger" data-action="2" name="btn-acao" value="<?=$item->produto->produto_id?>">
                                Excluir
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>
                        </td> 
                    </tr> 
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="5">Nenhum registro encontrado</td></tr> 
                <?php } ?>
            </tbody> 
        </table>
    </form>
</fieldset>

<div class="modal fade" tabindex="-1" id="modal-confirmacao" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content text-center">
            <div class="modal-header"> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button> 
                <h4 class="modal-title" id="mySmallModalLabel">Confirmação</h4> 
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja excluir este produto?</p>
            </div>
            <div class="modal-footer"> 
                <button type="button" class="btn btn-default alg-left-btn" data-dismiss="modal">Cancelar</button> 
                <button type="button" name="btn-excluir" class="btn btn-danger">Excluir</button> 
            </div>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('button[name="btn-acao"]').click(function() {
            
            if($(this).attr('data-action') == '2') {
                
                $('#modal-confirmacao').find('button[name="btn-excluir"]').attr('value', $(this).val());
                
                $('#modal-confirmacao').modal({
                    backdrop: 'static',
                    show: true
                });
                return;
            }
            
            var params = {
                action: '<?=$this->url->get('produto/editar')?>',
                method: 'POST',
                inputs: {
                    produto_id: $(this).val()
                }
            }
            
            App.createFormSubmit(params);
        });
        
        $('button[name="btn-excluir"]').click(function() {
            var params = {
                action: '<?=$this->url->get('produto/excluir')?>',
                method: 'POST',
                inputs: {
                    produto_id: $(this).val()
                }
            }

            App.createFormSubmit(params);
        });
    });
</script>
    