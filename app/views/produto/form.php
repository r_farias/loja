<div class="row">
    <fieldset>
        <legend>Cadastro de Produto</legend>
        <form id="form-produto" action="<?=$this->url->get('produto/salvar')?>" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="control-label"  for="descricao">Descrição</label>
                        <?=$this->tag->textField(["descricao", "class" => 'form-control', "placeholder" => "Descrição do produto"])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <?php
                            echo $this->tag->select([
                                'categoria_id',
                                $categorias,
                                "using" => array("categoria_id", "nome"),
                                "class" => 'form-control',
                                'useEmpty' => true,
                                'emptyText' => 'Selecione',
                                'emptyValue' => '',
                            ]);
                        ?>
                        <?php if(!count($categorias)) { ?>
                        <br/><a href="<?=$this->url->get("produto-categoria/cadastro")?>">Nenhuma categoria cadastrada, clique aqui para cadastrar</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="caracteristicas">Caracteristicas</label>
                        <?=$this->tag->textArea(["caracteristicas", "placeholder" => "Caracteristicas do produto", "class" => 'form-control', "rows" => 10, "cols" => 10])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="valor">Valor R$</label>
                        <?=$this->tag->textField(["valor", "class" => 'form-control mask-money'])?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="img">Imagem do Produto</label>
                        <?=$this->tag->fileField(["img", "accept" => 'image/*'])?>
                        <p class="help-block">Selecione uma imagem do produto para exibir</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <?php if($img != null && file_exists('files/produto/' . $img)) { ?>
                    <p>Imagem atual</p>
                    <img style="height: 100px" src="<?=$this->url->getBaseUri() . 'files/produto/' . $img?>">
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a href="<?=$this->url->get('produto/listar')?>" class="btn btn-default">Voltar</a>
                    <button class="btn btn-success" type="submit">Salvar</button>
                </div>
            </div>
            <?=$this->tag->hiddenField("tipo")?>
            <?=$this->tag->hiddenField("produto_id")?>
        </form>
    </fieldset>
</div>
<script>
    $(function() {
        $('.mask-money').maskMoney({
            'thousands' : '.',
            'decimal' : ',',
            'prefix' : 'R$'
        });
        
        jQuery.validator.addMethod('imagem', function (value, element, param) {
            if($(element).val() != "") {
                if ((($(element)[0].files[0].size) / 1024 /1024) > 1) {
                    return false;
                }
            }
            return true;
            
        }, 'O tamanho da imagem deve ser no maximo 1MB');
        
        var formProduto = {
            descricao: {
                required: true
            },
            valor: {
                required: true
            },
            categoria_id: {
                required: true
            },
            img: {
                imagem: true
            }
        }
        
         $('#form-produto').validate({
            rules: formProduto,
            messages: {
                descricao: {
                    required: 'Digite a descrição do produto',
                },
                valor: {
                    required: 'Informe um valor',
                },
                categoria_id: {
                    required: 'Informe uma categoria',
                }
            },
            highlight: function(element) {
                $(element).closest('div.form-group').addClass("has-error");
                $(element).closest('div.form-group').find('label:first').addClass("control-label");
            },
            unhighlight: function(element) {
                $(element).closest('div.form-group').removeClass("has-error");
            }
        });
    })
</script>