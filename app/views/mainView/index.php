<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Sistema de cadastro de produtos">
        <meta name="author" content="Rodrigo Farias">
        <title>Loja</title>
        <link rel="icon" type="image/png" href="<?=$this->url->getBaseUri() . 'img/faviconRF.png'?>">
        <link href="<?=$this->url->getBaseUri() . 'css/layout.css'?>" rel="stylesheet">
        <link href="<?=$this->url->getBaseUri() . 'vendor/bootstrap/css/bootstrap.min.css'?>" rel="stylesheet">
        <link href="<?=$this->url->getBaseUri() . 'vendor/bootstrap/css/bootstrap-theme.min.css'?>" rel="stylesheet">
        <script type="text/javascript" src="<?=$this->url->getBaseUri() . 'vendor/jquery/jquery.min.js'?>"></script>
        <script type="text/javascript" src="<?=$this->url->getBaseUri() . 'vendor/jquery/jquery.maskMoney.min.js'?>"></script>
        <script type="text/javascript" src="<?=$this->url->getBaseUri() . 'vendor/jquery/jquery.validate.min.js'?>"></script>
    </head>
    <body>
        <main>
            <nav class="navbar navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?=$this->url->get('index')?>">Loja RF</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Produto<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=$this->url->get('produto/cadastro')?>">Cadastrar</a></li>
                                    <li><a href="<?=$this->url->get('produto/listar')?>">Listar</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categoria<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?=$this->url->get('produto-categoria/cadastro')?>">Cadastrar</a></li>
                                    <li><a href="<?=$this->url->get('produto-categoria/listar')?>">Listar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container container-content" role="main">
                <?= $this->flash->output() ?>
                <?= $this->getContent()?>
            </div>
        </main>
        <div class="container" role="main">
            <footer>
                &copy <?=date('Y')?> Rodrigo Farias
            </footer>
        </div>
        <script type="text/javascript" src="<?=$this->url->getBaseUri() . 'vendor/bootstrap/js/bootstrap.min.js'?>"></script>
        <script type="text/javascript" src="<?=$this->url->getBaseUri() . 'js/App.js'?>"></script>
    </body>
</html>