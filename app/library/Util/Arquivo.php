<?php

namespace Library\Util;

/**
 * Operações que auxiliam no tratamento de arquivos.
 */
class Arquivo
{
    /**
     * Retorna a extensão do arquivo, com base no nome.
     * 
     * @param string $nomeArq Nome do arquivo
     * @return string
     */
    public static function getExtensao($nomeArq)
    {
        $arrFile = explode(".", $nomeArq);
        return end($arrFile);
    }

    /**
     * Gera um nome aleatório para um arquivo.
     * 
     * @param string $nomeArq Nome do arquivo (preserva extensão)
     * @return $string
     */
    public static function geraNomeArquivo($nomeArq = '')
    {
        $fileName = md5(uniqid(rand(), true));

        $ext = self::getExtensao($nomeArq);

        if (!empty($ext))
            $fileName .= '.' . $ext;

        return $fileName;
    }
}
