<?php

namespace Infra\Util;

/**
 * Funções de formatação.
 */
class Formatacao
{
    
    /**
     * Retorna somente números.
     * 
     * @param string $valor
     * @return string
     */
    public static function somenteNumeros($valor)
    {
        return preg_replace('/[^0-9]/is', '', $valor);
    }
    
    /**
     * Remove espaços, tabs e quebras de linha no começo, final e duplicados no meio da string.
     * 
     * @param string $valor
     * @return string
     */
    public static function removeEspacosAdicionais($valor)
    {
        return trim(preg_replace('/\s+/', ' ', $valor));
    }
    
}
