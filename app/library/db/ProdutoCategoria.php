<?php

namespace Library\Db;

class ProdutoCategoria extends \Library\Db\Base
{
    const TIPO_CADASTRO = 1;
    const TIPO_UPDATE = 2;
    
    public static function salvar($params, $tipo)
    {
        if ($tipo == self::TIPO_CADASTRO) {
            $produtoCategoria = new \Model\ProdutoCategoria();
        } else {
            $produtoCategoria = \Model\ProdutoCategoria::findFirstByCategoriaId($params['categoria_id']);
        }
        
        unset($params['tipo']);
        
        $produtoCategoria->assign($params);
        
        return ['save' => $produtoCategoria->save(), 'messages' => $produtoCategoria];
    }
    
    public static function listar($params = [])
    {
        $builder = self::getInstance();
        $builder->columns("produto_categoria.*");
        $builder->from(["produto_categoria" => "Model\ProdutoCategoria"]);
        
        if (array_key_exists('nome', $params) && trim($params['nome']) != "")
            $builder->andWhere('produto_categoria.nome LIKE :nome:', ['nome' => "%" . $params['nome'] . "%"]);
        
        $result = $builder->getQuery()->execute();
        return $result;
    }
}