<?php

namespace Library\Db;

Class Base
{
    public static function getInstance() 
    {
        return \Phalcon\DI::getDefault()->get('modelsManager')->createBuilder();
    }
}