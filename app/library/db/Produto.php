<?php

namespace Library\Db;

class Produto extends \Library\Db\Base
{
    const TIPO_CADASTRO = 1;
    const TIPO_UPDATE = 2;
    
    public static function salvar($params, $tipo, $files = [])
    {
        if ($tipo == self::TIPO_CADASTRO) {
            $produto = new \Model\Produto();
        } else {
            $produto = \Model\Produto::findFirstByProdutoId($params['produto_id']);
            $imgDeletar = $produto->getImg();
            unset($params['produto_id']);
        }
        
        unset($params['tipo']);
        
        //se possuí arquivo para fazer upload
        if (count($files) > 0) {
            foreach ($files as $file) {
                $nameFile = \Library\Util\Arquivo::geraNomeArquivo($file->getName());
                if ($file->moveTo('files/produto/' . $nameFile)) {
                    $params['img'] = $nameFile;
                    
                    if($imgDeletar != null && file_exists('files/produto/' . $imgDeletar))
                        unlink('files/produto/' . $imgDeletar);
                }
            }
        }
        
        if(strlen(trim($params['caracteristicas'])) == 0) {
            unset($params['caracteristicas']);
        }
        
        $produto->assign($params);
        
        return ['save' => $produto->save(), 'messages' => $produto];
    }
    
    public static function listar($params = [])
    {
        $builder = self::getInstance();
        $builder->columns("produto.*, produto_categoria.nome");
        $builder->from(["produto" => "Model\Produto"]);
        $builder->join("Model\ProdutoCategoria", "produto_categoria.categoria_id = produto.categoria_id", "produto_categoria");
        
        if (array_key_exists('descricao', $params) && trim($params['descricao']) != "")
            $builder->andWhere('produto.descricao LIKE :descricao:', ['descricao' => "%" . $params['descricao'] . "%"]);
        
        if (array_key_exists('caracteristicas', $params) && trim($params['caracteristicas']) != "")
            $builder->andWhere('produto.caracteristicas LIKE :caracteristicas:', ['caracteristicas' => "%" . $params['caracteristicas'] . "%"]);
        
        if (array_key_exists('valor', $params) && trim($params['valor']) != "")
            $builder->andWhere('produto.valor LIKE :valor:', ['valor' => "%" . $params['valor'] . "%"]);
        
        if (array_key_exists('categoria_id', $params) && $params['categoria_id'] != "todos")
            $builder->andWhere('produto_categoria.categoria_id = :categoria_id:', ['categoria_id' => $params['categoria_id']]);
        
        $result = $builder->getQuery()->execute();
        return $result;
    }
    
    public static function replaceValor($valor)
    {
        $valor = str_replace('R$', '', $valor);
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        
        return $valor;
    }
}
