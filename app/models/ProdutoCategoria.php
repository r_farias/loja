<?php

namespace Model;

class ProdutoCategoria extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $categoria_id;

    /**
     *
     * @var string
     */
    protected $nome;

    /**
     * Method to set the value of field categoria_id
     *
     * @param integer $categoria_id
     * @return $this
     */
    public function setCategoriaId($categoria_id)
    {
        $this->categoria_id = $categoria_id;

        return $this;
    }

    /**
     * Method to set the value of field nome
     *
     * @param string $nome
     * @return $this
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Returns the value of field categoria_id
     *
     * @return integer
     */
    public function getCategoriaId()
    {
        return $this->categoria_id;
    }

    /**
     * Returns the value of field nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    public function getSource()
    {
        return 'produto_categoria';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'categoria_id' => 'categoria_id', 
            'nome' => 'nome'
        );
    }
    
    public static function mensagensValidacao($mensagens)
    {
        $arrAux = [];
        foreach ($mensagens as $mensagem) {
            switch ($mensagem->getType()) {
                case "PresenceOf":
                    $arrAux[] = "O campo " . $mensagem->getField() . " é obrigatório";
                    break;
            }
        }
        
        return $arrAux;
    }

}
