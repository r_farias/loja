<?php

namespace Model;

class Produto extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $produto_id;

    /**
     *
     * @var integer
     */
    protected $categoria_id;

    /**
     *
     * @var string
     */
    protected $descricao;

    /**
     *
     * @var string
     */
    protected $caracteristicas;

    /**
     *
     * @var double
     */
    protected $valor;
    
    /**
     *
     * @var string
     */
    protected $img;

    /**
     * Method to set the value of field produto_id
     *
     * @param integer $produto_id
     * @return $this
     */
    public function setProdutoId($produto_id)
    {
        $this->produto_id = $produto_id;

        return $this;
    }

    /**
     * Method to set the value of field categoria_id
     *
     * @param integer $categoria_id
     * @return $this
     */
    public function setCategoriaId($categoria_id)
    {
        $this->categoria_id = $categoria_id;

        return $this;
    }

    /**
     * Method to set the value of field descricao
     *
     * @param string $descricao
     * @return $this
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;

        return $this;
    }

    /**
     * Method to set the value of field caracteristicas
     *
     * @param string $caracteristicas
     * @return $this
     */
    public function setCaracteristicas($caracteristicas)
    {
        $this->caracteristicas = $caracteristicas;

        return $this;
    }

    /**
     * Method to set the value of field valor
     *
     * @param double $valor
     * @return $this
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }
    
    /**
     * Method to set the value of field img
     *
     * @param string $img
     * @return $this
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Returns the value of field produto_id
     *
     * @return integer
     */
    public function getProdutoId()
    {
        return $this->produto_id;
    }

    /**
     * Returns the value of field categoria_id
     *
     * @return integer
     */
    public function getCategoriaId()
    {
        return $this->categoria_id;
    }

    /**
     * Returns the value of field descricao
     *
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Returns the value of field caracteristicas
     *
     * @return string
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Returns the value of field valor
     *
     * @return double
     */
    public function getValor()
    {
        return 'R$' . number_format($this->valor, 2, ',', '.');
    }
    
    /**
     * Returns the value of field img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    public function getSource()
    {
        return 'produto';
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'produto_id' => 'produto_id', 
            'categoria_id' => 'categoria_id', 
            'descricao' => 'descricao', 
            'caracteristicas' => 'caracteristicas', 
            'valor' => 'valor',
            'img' => 'img'
        );
    }
    
    public static function mensagensValidacao($mensagens)
    {
        $arrAux = [];
        foreach ($mensagens as $mensagem) {
            switch ($mensagem->getType()) {
                case "PresenceOf":
                    $arrAux[] = "O campo " . $mensagem->getField() . " é obrigatório";
                    break;
            }
        }
        
        return $arrAux;
    }

}
