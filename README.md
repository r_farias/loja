# README #

Ambiente de desenvolvimento utilizado.
Sistema Operacional Windows10 64BITS.
WampServer 3.0.6 64BIT
Apache 2.4.23
PHP 5.6.25
MySQL 5.7.14
Phalcon Framework 3.2.2
https://phalconphp.com/pt/
Lembre-se: fazer alteração das configurações de banco caso seus dados de acesso estejam diferentes.
O diretório que contém o projeto por defult deve-se chamar loja, caso queira modificar o nome, uma alteração no arquivo de configuração deve ser feita.
arquivo app/config/config.php
A chave do array baseUri está da seguinte forma 
'baseUri' => '//' . $_SERVER['HTTP_HOST'] . '/loja/public/'
com a alteração do nome do diretório deve ficar assim 'baseUri' => '//' . $_SERVER['HTTP_HOST'] . '/seudiretorio/public/'