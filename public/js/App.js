var App = {};

    //form
App.createFormSubmit = function(params) {
    var form = this.montarForm(params);
    $('body').append(form);
    $('form[name="create-form"]').submit();
}

App.montarForm = function(params) {

    //se tem inputs
    if (typeof params.action == 'undefined') {
        console.info('Para utilizar o create form o parametro action é obrigatorio');
        return false;
    }

    if (typeof params.method == 'undefined') {
        console.info('Para utilizar o create form o parametro method é obrigatorio');
        return false;
    }

    var form = '<form name="create-form" action="' + params.action + '" method="' + params.method + '">';

    if (typeof params.inputs == 'object') {
        var inputs = params.inputs;
        //adiciona os inputs para o formulario
        for(var i in inputs) {
            form += '<input hidden name="' + i + '" value="' + inputs[i] + '">';
        }
    }

    form += '</form>';
    return form;
}

App.init = function() {
    
}();